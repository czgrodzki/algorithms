#include <iostream>
using namespace std;

void makeAHeap(int arr[], int n, int i);
void heapSort(int arr[], int n);

int main()
{
    int tab[] = {123, 323,4 , -3123, 53, 53234, 234, 43, 0, 22, -232, -1, 23 };
    int size = 13;

    heapSort(tab, size);
    for (int i = 0; i < size; i++)
        cout << tab[i] << " ";
    cout << endl;

}

void makeAHeap(int arr[], int n, int i) {
    int max = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    if (left < n && arr[left] > arr[max])
        max = left;

    if (right < n && arr[right] > arr[max])
        max = right;

    if (max != i) {
        swap(arr[i], arr[max]);

        makeAHeap(arr, n, max);
    }
}

void heapSort(int arr[], int n) {

    for (int i = n / 2 - 1; i >= 0; i--)
        makeAHeap(arr, n, i);

    for (int i = n - 1; i >= 0; i--) {
    
        swap(arr[0], arr[i]);

        makeAHeap(arr, i, 0);
    }
}
