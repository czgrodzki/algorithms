#pragma once
#include <iostream>
using namespace std;

/*
 * quick sort
 */
void quickSort(int tab[], int down, int up);
int split(int arr[], int min, int high);
void swap(int* a, int* b);

int main() {

    int tab[] = {123, 323,4 , -3123, 53, 53234, 234, 43, 0, 22, -232, -1, 23 };
    int size = 13;

    quickSort(tab, 0, size);
    for (int i = 0; i < size; i++)
        cout << tab[i] << " ";
    cout << endl;

}

void quickSort(int tab[], int down, int up) {

    if (down < up) {
        int s = split(tab, down, up);

        quickSort(tab, down, s - 1);
        quickSort(tab, s + 1, up);
    }

}

int split(int tab[], int down, int up) {
    int s = tab[up];
    int i = (down - 1);

    for (int j = down; j <= up - 1; j++) {

        if (tab[j] <= s) {
            i++;
            swap(&tab[i], &tab[j]);
        }
    }
    swap(&tab[i + 1], &tab[up]);
    return (i + 1);
}

void swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}