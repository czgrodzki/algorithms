#pragma once
#include <iostream>
using namespace std;

/*
 * całkowanie numeryczne metodą Monte-Carlo
 */
double integral(double a, double b, double n);

int main() {

    double a, b, n;

    cout << "Type a " << endl;
    cin >> a;

    cout << "Type b " << endl;
    cin >> b;

    cout << "Type n " << endl;
    cin >> n;

    cout << "Result: " << integral(a, b, n) << endl;
}

double f(double x) {
    return (x * x * x) + 2 * (x * x) - 5 * x + 3;
}

double randd() {
    return 1.0*(double)rand() / (RAND_MAX+1);
}

double integral(double a, double b, double n) {

    double sum;

    for (int i = 1; i <= n; i=i+1) {
        sum += f(a + (b-a) * randd());
    }
    return sum*(b-a)/n;
}
