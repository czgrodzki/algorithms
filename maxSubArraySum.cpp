#include<iostream>
#include<climits>
using namespace std;

void maxSubArraySum(int tab[], int size);

int main() {

    int tab[] = {123, 323,4 , -3123, 53, 53234, 234, 43, 0, 22, -232, -1, 23 };
    int size = 13;

    maxSubArraySum(tab, size);

}

void maxSubArraySum(int tab[], int size) {
    int maxSoFar = INT_MIN, maxHere = 0,
            start =0, end = 0, s=0;

    for (int i=0; i< size; i++ ) {
        maxHere += tab[i];

        if (maxSoFar < maxHere) {
            maxSoFar = maxHere;
            start = s;
            end = i;
        }

        if (maxHere < 0) {
            maxHere = 0;
            s = i + 1;
        }
    }
    cout << "Max: "<< maxSoFar << endl;
    cout << "Starts at:  "<< start << endl;
    cout << "Ends at: "<< end << endl;
}
